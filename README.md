# MC Payment Coding Test - Java

Project ini dibuat dengan NetBeans IDE, jika sudah punya NetBeans bisa langsung di clone dan running saja. Jika tidak menggunakan NetBeans IDE bisa melihat code yang saya buat pada folder `src/codechallange`

## Cara Running di NetBeans
Soal yang di berikan ada 3, jadi saya buat 3 file java yang berbeda dengan setiap file mewakilkan nomor pada soal.

* Run file Question1.java (Untuk Soal no 1)

Klik kanan pada file Question1.java lalu pilih run file 

![2021-04-16_10h08_19](/uploads/1dab193449d11df18650d8f0ce520246/2021-04-16_10h08_19.png)

Ouput yang dihasilkan akan seperti ini

![2021-04-16_10h14_49](/uploads/7687ddf4001befd4866ae610b438eef1/2021-04-16_10h14_49.png)

* Run File Question2.java (Untuk Soal no 2)

Klik kanan pada file Question2.java lalu pilih run file 

![2021-04-16_10h25_11](/uploads/dfbcf21bf1760cafea2d5f4fb46d50df/2021-04-16_10h25_11.png)

Ouput yang dihasilkan akan seperti ini

![2021-04-16_10h25_24](/uploads/c7ba03f3ba2c887668dd25d68269df84/2021-04-16_10h25_24.png)

* Run File Question3.java (Untuk Soal no 3)

Klik kanan pada file Question3.java lalu pilih run file 

![2021-04-16_10h28_15](/uploads/a5d2417207e5aacd08655a971cfd7c11/2021-04-16_10h28_15.png)

Ouput yang dihasilkan akan seperti ini

![2021-04-16_10h28_26](/uploads/ced08896c704df0f86ca8e97faff3d5b/2021-04-16_10h28_26.png)

## Catatan 

Pada Project ini saya membuat Helper untuk validasi value dalam array, ada 2 method dalam Helpers.java yaitu :

**1. doesArrayContainNegative** 

Method ini digunakan untuk cek value dalam array apakah mengandung value negatif. jika iya maka maka return `true`, jika tidak maka return `false`.
Method ini digunakan pada Question1.java

```bash
    public static boolean doesArrayContainNegative(List<Integer> array){
        for(int arr : array){
   	if(arr < 0) return true;
        }
       return false;
    }
```

**2. doesArrayContainX** 

Method ini digunakan untuk cek value dalam array apakah mengandung value dari variable `x`. jika iya maka maka return `true`, jika tidak maka return `false`. Method ini digunakan pada Question2.java

```bash
    public static boolean doesArrayContainX(List<Integer> array,int x){
        for(int arr : array){
   	if(arr == x) return true;
        }
       return false;
    }
```
