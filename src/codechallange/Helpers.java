
package codechallange;

import java.util.List;

/**
 *
 * @author Ade Guntur
 * 
 */

public class Helpers {
    
     public static boolean doesArrayContainNegative(List<Integer> array){
        for(int arr : array){
   	if(arr < 0) return true;
        }
       return false;
    }
     
    public static boolean doesArrayContainX(List<Integer> array,int x){
        for(int arr : array){
   	if(arr == x) return true;
        }
       return false;
    }
}
