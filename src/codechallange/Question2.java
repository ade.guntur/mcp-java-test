package codechallange;

/**
 *
 * @author Ade Guntur
 * 
 */

import java.util.ArrayList;
import java.util.List;

public class Question2 extends Helpers{

    public static void main(String[] args) {
        // TODO code application logic here
        int [] nums = new int[]{ 1,2,3,4}; 
        int x = 4;
 
        // panggil function getResultArray untuk mendapatkan hasil angka yang jika dibagi dengan angka pada array nums 
        // hasil array nya tidak mengandung angka pada variable x

        List<Integer> result = getResultArray(nums,x);
        
        System.out.println(result);

    }
    
    private static List<Integer> getResultArray(int[] nums,int x){
    
        // buat Objek ArrayList kosong
        List<Integer> result = new ArrayList<Integer>();
        
         // lakukan perulangan untuk mendapatkan angka yang mau di bagi 
        for(int i=0; i<nums.length;i++){

            // buat variable array temporary untuk menampung hasil pembagian

            List<Integer> temp = new ArrayList<Integer>();

             // laukakan perulangan untuk mendapatkan angka sebagai pembagi angka yang diatas
            for(int j=0;j<nums.length;j++){

               // lakukan pembagian dan masukan hasil pembagian pada array temporary
               temp.add(nums[i]/nums[j]);

            }
            
            // check apakah pada array temporary ada/mengandung value pada variable x
            if(!doesArrayContainX(temp,x)){

                // jika tidak mengandung value pada variable x, maka masukan angka yang dibagi kedalam ArrayList result
                result.add(nums[i]);
            } 
        }
 
         // lakukan pengembalian dari pemanggilan function ini dengan value pada array result
        return result;
        
    }
    
}
