/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codechallange;

/**
 *
 * @author Ade Guntur
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Question3 {
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        String word = "souvenir loud four lost";
        int x = 4;

        // panggil function getStringHasLengthX untuk mendapatkan hasil value kata pada word yang jumlah huruf nya senilai pada variable x

        List<String> result = getStringHasLengthX(word,x);
        
        System.out.println(result);
    }
    
    private static List<String> getStringHasLengthX(String word,int x){
        
        // lakukan split whitespace pada variable word dan ubah menjadi bentuk array
        String[] wordToArray = word.split(" "); 

        // buat Objek ArrayList kosong
        List<String> result = new ArrayList<String>();
        
        // lakukan perulangan pada array wordToArray
        for(int i=0;i<wordToArray.length;i++){

            // check apakah value dari array wordToArray mempunyai panjang yang sama dengan value pada variable x  
            if(wordToArray[i].length() == x){

                // jika iya, maka masukan value wordToArray pada ArrayList result
                result.add(wordToArray[i]);
            }
        }

        // lakukan pengembalian dari pemanggilan function ini dengan value pada array result
        return result;
        
    }
      
}
