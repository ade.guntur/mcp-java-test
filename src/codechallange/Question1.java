package codechallange;

/**
 *
 * @author Ade Guntur
 * 
 */

import java.util.ArrayList;
import java.util.List;

public class Question1 extends Helpers{

    public static void main(String[] args) {
        // TODO code application logic here
        
        int [] nums = new int[]{ 3,1,4,2}; 
        
        // panggil function getResultArray untuk mendapatkan hasil angka yang jika dikurangi dengan angka pada array nums 
        // hasil array nya tidak mengandung angka kurang dari 0 atau tidak mengandung value negatif

        List<Integer> result = getResultArray(nums);
        
        System.out.println(result);

    }
    
    private static List<Integer> getResultArray (int [] nums){

        // buat Objek ArrayList kosong
        List<Integer> result = new ArrayList<Integer>();
        
        // lakukan perulangan untuk mendapatkan angka yang mau di kurangi 
        for(int i=0; i<nums.length;i++){

            // buat variable array temporary untuk menampung hasil pengurangan
            List<Integer> temp = new ArrayList<Integer>();

            // laukakan perulangan untuk mendapatkan angka sebagai pengurang angka yang diatas
            for(int j=0;j<nums.length;j++){
               
               // lakukan pengurangan dan masukan hasil pengurangan pada array temporary
               temp.add(nums[i]-nums[j]);

            }

            // check apakah pada array temporary ada value negatif
            if(!doesArrayContainNegative(temp)){

                // jika tidak ada value negatif, maka masukan angka yang dikurangi kedalam ArrayList result
                result.add(nums[i]);
            }
             
        }
        
        // lakukan pengembalian dari pemanggilan function ini dengan value pada array result
        return result;
    }
    
}
